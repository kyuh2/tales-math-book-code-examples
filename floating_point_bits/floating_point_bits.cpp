#include <iostream>
#include <cstdlib>
#include <cassert>
#include <limits>
#include <bitset>
#include <string>


constexpr uint32_t EXPONENT_START = 31;
constexpr uint32_t EXPONENT_END = 23;

// How we'll put messages on screen
void print_str(const std::string& s)
{
    printf(s.c_str());
    printf("\n");
    fflush(NULL);
}

int main(int argc, char** argv)
{
    assert(sizeof(float) == 4);
    assert(sizeof(float) == sizeof(uint32_t));

    if (!std::numeric_limits<float>::is_iec559) {
        print_str(std::string()
                  + "Your computer stores floating-point values in a way "
                  + "this program doesn't expect (\"IEEE 754\"). "
                  + "Your results may be weird.");
    }

    if (argc < 2) {
        print_str("Enter a number (e.g. 3.0)");
        return EXIT_FAILURE;
    }

    float f = std::stof(argv[1]);

    print_str("Number: " + std::to_string(f));

    float* pf = &f;
    uint32_t* pi = reinterpret_cast<uint32_t*>(pf);
    uint32_t val_i = *pi;

    ///////////////

    // grab the first (leftmost) bit, which is the "sign bit"
    uint32_t sign_val = val_i & (1 << EXPONENT_END);
    // sign_val will either be 
    //      0x80000000,
    //          i.e. 10000000 00000000 00000000 00000000
    // or
    //      0x00000000,
    //          i.e. 00000000 00000000 00000000 00000000
    if (sign_val) {
        print_str("sign bit of 1  (negative)");
    } else {
        print_str("sign bit of 0  (non-negative)");
    }


    // "mask" to get the "exponent" part.
    // we basically want
    // 01111111 10000000 00000000 00000000
    // 
    // which is conveniently
    // 10000000 00000000 00000000 00000000
    // - (minus)
    // 00000000 10000000 00000000 00000000
    constexpr uint32_t exponent_mask = static_cast<uint32_t>(1 << EXPONENT_START)
                                        - static_cast<uint32_t>(1 << EXPONENT_END);

    uint32_t exponent_rawval = (val_i & exponent_mask) >> EXPONENT_END;

    std::string exponent_bitstring =
        std::bitset<EXPONENT_START - EXPONENT_END>(exponent_rawval).to_string();

    print_str("Exponent bits: " + exponent_bitstring);

    print_str("Exponent raw val: " + std::to_string(exponent_rawval));

    print_str("Exponent power of 2 (above minus 127): "
              + std::to_string(static_cast<int32_t>(exponent_rawval) - 127));


    // "mask" to get the significand bits.
    // we basically want
    // 00000000 01111111 11111111 11111111
    // 
    // which is conveniently
    // 00000000 10000000 00000000 00000000
    // - (minus)
    // 00000000 00000000 00000000 00000001
    constexpr uint32_t significand_mask = static_cast<uint32_t>(1 << EXPONENT_END) - static_cast<uint32_t>(1);
    uint32_t significand_rawval = (val_i & significand_mask);

    std::string significand_bitstring = std::bitset<EXPONENT_END>(significand_rawval).to_string();

    print_str("Significand bits: " + significand_bitstring);
    print_str("Multiplier (in binary): 1." + significand_bitstring);


    return EXIT_SUCCESS;
}
