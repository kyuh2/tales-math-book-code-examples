

#include <cctype>
#include <string>


namespace {

    // todo: string_view
    std::string rstrip(const std::string& str)
    {
        auto end_it = str.rbegin();
        while (std::isspace(*end_it)) {
            ++end_it;
        }
        return std::string(str.begin(), end_it.base());
    }

    std::string lstrip(const std::string& str)
    {
        auto start_it = str.begin();
        while (std::isspace(*start_it)) {
            ++start_it;
        }
        return std::string(start_it, str.end());
    }

    int extract_numerator_from_str(const std::string& str)
    {
        std::string clean_str = rstrip(str);
        return std::stoi(clean_str);
    }

    int extract_denominator_from_str(const std::string& str)
    {
        std::string clean_str = lstrip(str);
        return std::stoi(clean_str);
    }


    // here, assume for now a >= 0, b >= 0
    template<typename IType>
    IType gcd(IType a, IType b) {
        while (b) {
            IType t = b;
            b = a % b;
            a = t;
        }
        return a;
    }

    template<typename IType>
    IType absval(IType x) {
        return x >= 0L ? x : -x;
    }

    template<typename IType>
    IType signval(IType x) {
        return x >= 0L ? 1L : -1L;
    }

}

// Parse a string like "3 / 5"
// - template-specialized to int only
template<>
Fraction<int>::Fraction(const std::string& str)
{
    if (str.length() == 0)
        throw "invalid string (empty)";

    // find the slash
    size_t slash_pos = str.find("/");
    if (slash_pos == std::string::npos) {
        // no slash found - assume a numerator only
        num_ = extract_numerator_from_str(str);
        denom_ = 1;
    } else {

        // make sure there are no more slashes
        size_t another_slash_pos = str.find("/", slash_pos+1);
        if (another_slash_pos != std::string::npos) {
            throw "invalid string (multiple slashes found)";
        }

        // todo: replace with string_view
        auto n = extract_numerator_from_str(str.substr(0, slash_pos-1));
        auto d = extract_denominator_from_str(str.substr(slash_pos+1));

        num_ = n;
        denom_ = d;
    }
}


template<typename IType>
Fraction<IType>::Fraction(IType n, IType d, bool simplify)
{
    if (simplify) {
        IType abs_n = ::absval(n);
        IType abs_d = ::absval(d);
        IType sign_n = ::signval(n);
        IType sign_d = ::signval(d);

        // first handle the negative signs.
        // denominator will always be positive,
        // numerator will hold the "total sign"
        n = n * sign_n * sign_d;
        d = abs_d;

        IType g = ::gcd<IType>(abs_n,d);
        n /= g;
        d /= g;
    }
    num_ = n;
    denom_ = d;
}

template<typename IType>
Fraction<IType>::Fraction(const Fraction& f) : num_(f.num_), denom_(f.denom_)
{ }


template<typename IType>
Fraction<IType> Fraction<IType>::operator+(const Fraction& frac2) {
    IType a = num_;
    IType b = denom_;
    IType c = frac2.num_;
    IType d = frac2.denom_;

    IType num2 = a*d + b*c;
    IType denom2 = b*d;
    return Fraction(num2, denom2);
}

template<typename IType>
Fraction<IType> Fraction<IType>::operator-(const Fraction& frac2) {
    IType a = num_;
    IType b = denom_;
    IType c = frac2.num_;
    IType d = frac2.denom_;

    IType num2 = a*d - b*c;
    IType denom2 = b*d;
    return Fraction(num2, denom2);
}

template<typename IType>
Fraction<IType> Fraction<IType>::operator*(const Fraction& frac2) {
    IType a = num_;
    IType b = denom_;
    IType c = frac2.num_;
    IType d = frac2.denom_;

    return Fraction(a*c, b*d);
}

template<typename IType>
Fraction<IType> Fraction<IType>::operator/(const Fraction& frac2) {
    IType a = num_;
    IType b = denom_;
    IType c = frac2.num_;
    IType d = frac2.denom_;

    return Fraction(a*d, b*c);
}


template<typename IType>
Fraction<IType> Fraction<IType>::operator+(IType c) {
    IType a = num_;
    IType b = denom_;

    return Fraction(a + b*c, b);
}

template<typename IType>
Fraction<IType> Fraction<IType>::operator-(IType c) {
    IType a = num_;
    IType b = denom_;

    return Fraction(a - b*c, b);
}

template<typename IType>
Fraction<IType> Fraction<IType>::operator*(IType c) {
    IType a = num_;
    IType b = denom_;

    return Fraction(a*c, b);
}

template<typename IType>
Fraction<IType> Fraction<IType>::operator/(IType c) {
    IType a = num_;
    IType b = denom_;

    return Fraction(a, b*c);
}



template<typename IType>
ostream& operator<<(ostream& strm, const Fraction<IType>& frac)
{
    return strm << frac.num_ << " / " << frac.denom_;
}
