#ifndef FRACTION__HPP
#define FRACTION__HPP

#include <iostream>
#include <string>

using std::ostream;


template<typename IType = int>
class Fraction {
public:
    IType num_;
    IType denom_;

    Fraction(IType n, IType d, bool simplify=true);
    Fraction(const Fraction&);
    Fraction(const std::string& str);

    Fraction operator+(const Fraction& frac2);
    Fraction operator-(const Fraction& frac2);
    Fraction operator*(const Fraction& frac2);
    Fraction operator/(const Fraction& frac2);

    Fraction operator+(IType c);
    Fraction operator-(IType c);
    Fraction operator*(IType c);
    Fraction operator/(IType c);
};

template<typename IType>
Fraction(char const *) -> Fraction<int>;

template<typename IType>
ostream& operator<<(ostream& strm, const Fraction<IType>& frac);


#include "fraction.tpp"

#endif
