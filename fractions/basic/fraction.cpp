#include "fraction.hpp"

#include <cctype>
#include <string>


namespace {

    // todo: string_view
    std::string rstrip(const std::string& str)
    {
        auto end_it = str.rbegin();
        while (std::isspace(*end_it)) {
            ++end_it;
        }
        return std::string(str.begin(), end_it.base());
    }

    std::string lstrip(const std::string& str)
    {
        auto start_it = str.begin();
        while (std::isspace(*start_it)) {
            ++start_it;
        }
        return std::string(start_it, str.end());
    }

    int extract_numerator(const std::string& str)
    {
        std::string clean_str = rstrip(str);
        return std::stoi(clean_str);
    }

    int extract_denominator(const std::string& str)
    {
        std::string clean_str = lstrip(str);
        return std::stoi(clean_str);
    }

    // here, assume for now a >= 0, b >= 0
    int gcd(int a, int b) {
        while (b != 0) {
            int t = b;
            b = a % b;
            a = t;
        }
        return a;
    }

    int abs(int x) {
        return x >= 0 ? x : -x;
    }

    int sign(int x) {
        return x >= 0 ? 1 : -1;
    }

}


// Parse a string like "3 / 5"
Fraction::Fraction(const std::string& str)
{
    if (str.length() == 0)
        throw "invalid string (empty)";

    // find the slash
    size_t slash_pos = str.find("/");
    if (slash_pos == std::string::npos) {
        // no slash found - assume a numerator only
        num_ = extract_numerator(str);
        denom_ = 1;
    } else {

        // make sure there are no more slashes
        size_t another_slash_pos = str.find("/", slash_pos+1);
        if (another_slash_pos != std::string::npos) {
            throw "invalid string (multiple slashes found)";
        }

        // todo: replace with string_view
        auto n = extract_numerator(str.substr(0, slash_pos-1));
        auto d = extract_denominator(str.substr(slash_pos+1));

        num_ = n;
        denom_ = d;
    }
}






Fraction::Fraction(int n, int d, bool simplify)
{
    if (simplify) {
        // first handle the negative signs.
        // denominator will always be positive,
        // numerator will hold the "total sign"
        n = n * sign(n) * sign(d);
        d = ::abs(d);

        int g = gcd(::abs(n),d);
        n /= g;
        d /= g;
    }
    num_ = n;
    denom_ = d;
}

Fraction::Fraction(const Fraction& f) : num_(f.num_), denom_(f.denom_)
{ }


Fraction Fraction::operator+(const Fraction& frac2) {
    int a = num_;
    int b = denom_;
    int c = frac2.num_;
    int d = frac2.denom_;

    int num2 = a*d + b*c;
    int denom2 = b*d;
    return Fraction(num2, denom2);
}

Fraction Fraction::operator-(const Fraction& frac2) {
    int a = num_;
    int b = denom_;
    int c = frac2.num_;
    int d = frac2.denom_;

    int num2 = a*d - b*c;
    int denom2 = b*d;
    return Fraction(num2, denom2);
}

Fraction Fraction::operator*(const Fraction& frac2) {
    int a = num_;
    int b = denom_;
    int c = frac2.num_;
    int d = frac2.denom_;

    return Fraction(a*c, b*d);
}

Fraction Fraction::operator/(const Fraction& frac2) {
    int a = num_;
    int b = denom_;
    int c = frac2.num_;
    int d = frac2.denom_;

    return Fraction(a*d, b*c);
}



Fraction Fraction::operator+(int c) {
    int a = num_;
    int b = denom_;

    return Fraction(a + b*c, b);
}

Fraction Fraction::operator-(int c) {
    int a = num_;
    int b = denom_;

    return Fraction(a - b*c, b);
}

Fraction Fraction::operator*(int c) {
    int a = num_;
    int b = denom_;

    return Fraction(a*c, b);
}

Fraction Fraction::operator/(int c) {
    int a = num_;
    int b = denom_;

    return Fraction(a, b*c);
}




ostream& operator<<(ostream& strm, const Fraction& frac) 
{
    return strm << frac.num_ << " / " << frac.denom_;
}
