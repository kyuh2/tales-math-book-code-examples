#include <iostream>

#include "fraction.hpp"

using std::cout;
using std::endl;

int main() {
    Fraction f1(3, 5);
    Fraction f2(1, 2);
    cout << f1 + f2 << endl;

    Fraction f3(1, 3);
    Fraction f4(1, 6);
    cout << f3 + f4 << endl;

    Fraction f5(1, 2);
    Fraction f6(1, 6);
    cout << f5 - f6 << endl;

    cout << f1 * f2 << endl;
    cout << f1 / f2 << endl;

    //f5 += f6;
    //cout << f5 << endl;

    Fraction f7(f6);
    cout << f7 << endl;

    Fraction f8a = f6 + 2;
    Fraction f8b = f6 - 2;
    Fraction f8c = f6 * 2;
    Fraction f8d = f6 / 2;

    cout << f8a << endl;
    cout << f8b << endl;
    cout << f8c << endl;
    cout << f8d << endl;

    Fraction f9a("3 / 5");
    cout << f9a << endl;

    Fraction f9b("-250 / -430");
    cout << f9b << endl;

    //Fraction f9c("-250 / -56 / 2 / 5");
    //cout << f9c << endl;

    Fraction f9d(-250, -430);
    cout << f9d << endl;
}
