#include <iostream>
#include <string>

using std::ostream;

// temp
using std::size_t;

class Fraction {
public:
    int num_;
    int denom_;

    Fraction(int n, int d, bool simplify=true);
    Fraction(const Fraction&);
    Fraction(const std::string& str);

    Fraction operator+(const Fraction& frac2);
    Fraction operator-(const Fraction& frac2);
    Fraction operator*(const Fraction& frac2);
    Fraction operator/(const Fraction& frac2);

    Fraction operator+(int c);
    Fraction operator-(int c);
    Fraction operator*(int c);
    Fraction operator/(int c);
};

ostream& operator<<(ostream& strm, const Fraction& frac);
