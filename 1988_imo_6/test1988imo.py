"""
This script computes k for all patterns of a,b  (where a >= b, per WLOG),
and if k is an integer, i.e. (ab+1) | (a^2 + b^2),
shows its perfect square form.

Example command:
./test1988imo.py 1000
"""

import sys
import math


def main():
    if len(sys.argv) < 2:
        print("Needs a number")
        exit(1)

    max_a = int(sys.argv[1])
    epsilon = 1e-8     # 10^(-8) = 0.00000001

    for a in range(1, max_a):
        for b in range(1, a+1):
            n = a * a + b * b
            d = a * b + 1
            if n % d == 0:
                k = n/d

                # python's sqrt() will give a floating-point number.
                # this should be close to an integer, which we'll check.
                # (should be "very close", within our small "epsilon" distance)
                sq_k = math.sqrt(k)
                intsq_k = int(round(sq_k))
                if abs(sq_k - intsq_k) > epsilon:
                    print("This should never ever happen!")
                else:
                    print("a = %s, b = %s:    k = %s    = %s*%s" % (str(a), str(b), str(k), str(intsq_k), str(intsq_k)))

if __name__ == '__main__':
    main()
