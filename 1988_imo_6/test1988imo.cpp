#include <iostream>
#include <stdint.h>
#include <tgmath.h>
#include <cmath>

typedef uint64_t ulong;

int main(int argc, char** argv)
{
    if (argc < 2) {
        std::cout << "Enter n  (will try all pairs (a,b) of numbers a <= n, b <= n)" << std::endl;
        return EXIT_FAILURE;
    }

    ulong max_a = std::stoi(argv[1]);
    constexpr double epsilon = 1e-8;    // 10^(-8) = 0.00000001

    for (ulong a = 1; a <= max_a; a++) {
        for (ulong b = 1; b <= a; b++) {

            ulong num = a * a + b * b;
            ulong denom = a * b + 1;

            if (num % denom == 0) {
                ulong k = num / denom;

                // c++'s standard sqrt() will give a floating-point type
                // (here it'll be a "double", i.e. 64-bit floating point)
                // 
                // Like in the python script, 
                // this should be close to an integer, which we'll check.
                // (should be very close, within our small "epsilon" distance)
                double sq_k = sqrt( static_cast<double>(k) );
                double roundsq_k = std::round(sq_k);

                if (std::abs(roundsq_k - sq_k) > epsilon)
                    std::cout << "This should never happen!" << std::endl;
                else {
                    // cast so that we print without the decimal
                    ulong intsq_k = static_cast<ulong>(roundsq_k);

                    std::cout << "a = " << a << ", b = " << b << ":    k = " << k
                              << "    = " << intsq_k << "*" << intsq_k << std::endl;
                }
            }
        }
    }
    return EXIT_SUCCESS;
}
