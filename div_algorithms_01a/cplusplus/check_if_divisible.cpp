#include <iostream>
#include <string>

typedef uint64_t ulong;

// How we'll put messages on screen
void print_str(const std::string& s)
{
    printf(s.c_str());
    printf("\n");
    fflush(NULL);
}

//////////////////////////////////

// The "Important Part"

// Check if `a` divides `b`,
// using the algorithm from the book
//
// (Repeatedly increase k until a*k >= b.
//  Then:
//      - if it's > `b` then no, `a` does not divide b
//      = if it's equal to `b` then yes, `a` does divide b

bool check_if_divides(ulong a, ulong b)
{
    ulong candidate_k = 0;

    while (true) {
        ulong x = a * candidate_k;

        if (x == b) {
            return true;
        } else if (x > b) {
            return false;
        }

        candidate_k++;
    }
}

//////////////////////////////////

// Program starts running here

int main(int argc, char** argv)
{
    if (argc < 3) {
        print_str("Need at least 2 arguments");
        return EXIT_FAILURE;
    }

    // Todo - show or explain how argv is used

    ulong a = std::stoul(argv[1]);
    ulong b = std::stoul(argv[2]);

    if (check_if_divides(a, b)) {

        std::string msg =   std::string("Yes, ")
                          + std::to_string(a)
                          + " divides "
                          + std::to_string(b);

        print_str(msg);
    } else {

        std::string msg =   std::string("No, ")
                          + std::to_string(a)
                          + " does not divide "
                          + std::to_string(b);

        print_str(msg);
    }

    return EXIT_SUCCESS;
}
