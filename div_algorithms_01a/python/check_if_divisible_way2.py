###################################

# The "Important Part"

# Check if `a` divides `b`,
# using a slightly different method vs the
# original from the book.
#
# Why does this way work too?

def check_if_divides(a, b):
    current_product = 0

    while current_product < b:
        current_product = current_product + a

    if current_product == b:
        return True
    else:
        return False

###################################

# Stuff to help read in the inputs,
# and check that int input values are actual numbers

import sys

def exit_program_failure():
    sys.exit(1)

def is_string_a_counting_number(s):
    return str.isdigit(s)


###################################

# The program "starts" running real code here

if __name__ == '__main__':

    a_str = sys.argv[1]
    b_str = sys.argv[2]

    if not is_string_a_counting_number(a_str):
        print(f"Argument {a_str} is not a counting number")
        exit_program_failure()

    if not is_string_a_counting_number(b_str):
        print(f"Argument {b_str} is not a counting number")
        exit_program_failure()

    a = int(a_str)
    b = int(b_str)

    if check_if_divides(a, b):
        print(f"Yes, {a} divides {b}")
    else:
        print(f"No, {a} does not divide {b}")
