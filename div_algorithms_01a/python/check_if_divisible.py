###################################

# The "Important Part"

# Check if `a` divides `b`,
# using the algorithm from the book
# (repeated subtraction)

def check_if_divides(a, b):
    candidate_k = 0

    while True:
        x = a * candidate_k

        if x == b:
            return True
        elif x > b:
            return False

        # Otherwise, `x < b`  (i.e. `a*k < b`),
        # so bump up `k` by 1 and keep trying
        candidate_k = candidate_k + 1


###################################

# Stuff to help read in the inputs,
# and check that int input values are actual numbers

import sys

def exit_program_failure():
    sys.exit(1)

def is_string_a_counting_number(s):
    return str.isdigit(s)


###################################

# The program "starts" running real code here

if __name__ == '__main__':

    a_str = sys.argv[1]
    b_str = sys.argv[2]

    if not is_string_a_counting_number(a_str):
        print(f"Argument {a_str} is not a counting number")
        exit_program_failure()

    if not is_string_a_counting_number(b_str):
        print(f"Argument {b_str} is not a counting number")
        exit_program_failure()

    a = int(a_str)
    b = int(b_str)

    if check_if_divides(a, b):
        print(f"Yes, {a} divides {b}")
    else:
        print(f"No, {a} does not divide {b}")
