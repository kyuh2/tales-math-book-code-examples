#include <iostream>
#include <cstdlib>
#include <string>


// We could have made these take in
// `uint32_t` (forced-nonnegative) values.
// But we'd need an extra cast from the input,
// and `std::stoi` has no `uint32_t` equivalent
//
// Note that this function still works with input
// `x = 2`  (it will correctly say that 2 is
// a prime number).
// In this case, the for-loop will never be entered,
// and it will return `true`.

bool is_prime_number(int32_t x) {

    for (int32_t i = 2; i < x; i++) {
        if (x % i == 0) {
            return false;
        }
    }
    return true;
}

int main(int argc, char** argv) {

    if (argc < 2) {
        std::cout << "Enter a number (e.g. 3.0)" << std::endl;
        return EXIT_FAILURE;
    }

    int32_t value = std::stoi(argv[1]);

    // Exclude values less than 2
    // (including negative numbers)
    if (value < 2) {
        std::cout << "Input must be at least 2" << std::endl;
        return EXIT_FAILURE;
    }

    if (is_prime_number(value)) {
        std::cout << value << " is prime" << std::endl;
    } else {
        std::cout << value << " is *not* prime" << std::endl;
    }

    return EXIT_SUCCESS;
}
