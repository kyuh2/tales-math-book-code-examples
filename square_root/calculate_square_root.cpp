#include <iostream>
#include <string>

#include <cmath>

// Change this to 0 to not print the
// "intermediate sequence values"
// during the square-root calculation
// (and not even compile the "print code"
// into the executable program)
#define SHOULD_PRINT_STEPS 1

#if SHOULD_PRINT_STEPS
static unsigned running_sequence_index = 0;

#define PRINT_INTERMEDIATE_STEP(x) do { std::cout << "Square-root intermediate guess (a_" << running_sequence_index << "): " << x << std::endl; running_sequence_index++; } while (0);

#else
#define PRINT_INTERMEDIATE_STEP(x) do {} while (0);
#endif


// If you're writing code elsewhere and need
// an absolute-value function
// you can usually use std::abs(value)
// from header <cmath>
double absval(double x)
{
    return (x > 0.0) ? x : -x;
}

// The algorithm to compute the square root
// shown in Tales of Numbers, Chapter 3
//
// If you're writing code elsewhere and need
// a square root function,
// you can usually use std::sqrt(value)
// from header <cmath>
double squareroot(double a, double epsilon=1e-8)
{
    double x = a;
    double tmp;
    while (true) {
        tmp = 0.5 * (x + a / x);
        if (absval(tmp - x) < epsilon) {
            return x;
        }
        x = tmp;
        PRINT_INTERMEDIATE_STEP(x)
    }
}

int main(int argc, char **argv)
{
    if (argc < 2) {
        std::cout << "Enter a number (e.g. 3.0)" << std::endl;
        return EXIT_FAILURE;
    }

    double a = std::stof(argv[1]);
    double sqrt_a = squareroot(a);

    std::cerr << "sqrt(" << a << ") = " << sqrt_a << std::endl;
    return EXIT_SUCCESS;
}
