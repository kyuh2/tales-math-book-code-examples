
Code examples, simulations, labs, etc. for the tales math + computing book. 

These will mostly be in Python and C++.


Main repo for the book:
https://gitlab.com/kyuh2/tales-math-book




# Requirements

- python3

- gcc
	- Needs a relatively recent version -- this project is currently tested on g++ from gcc 8.3. Some examples will require c++17 features.)

- scons:  https://github.com/SCons/scons


