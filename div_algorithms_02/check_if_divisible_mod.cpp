#include <iostream>
#include <string>

// TODO comment on this
typedef uint64_t ulong;

// How we'll put messages on screen
void print_str(const std::string& s)
{
    printf(s.c_str());
    printf("\n");
    fflush(NULL);
}

//////////////////////////////////

// Check if `a` divides `b`,

bool check_if_divides_fastway(ulong a, ulong b)
{
    return b % a == 0;
}

//////////////////////////////////

// Program starts running here

int main(int argc, char** argv)
{
    if (argc < 3) {
        print_str("Need at least 2 arguments");
        return EXIT_FAILURE;
    }

    // Todo - show or explain how argv is used

    ulong a = std::stoul(argv[1]);
    ulong b = std::stoul(argv[2]);

    if (check_if_divides_fastway(a, b)) {

        std::string msg =   std::string("Yes, ")
                          + std::to_string(a)
                          + " divides "
                          + std::to_string(b);

        print_str(msg);
    } else {

        std::string msg =   std::string("No, ")
                          + std::to_string(a)
                          + " does not divide "
                          + std::to_string(b);

        print_str(msg);
    }

    return EXIT_SUCCESS;
}
